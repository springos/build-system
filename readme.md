# Spring OS Build system

This repository bootstraps all of Spring OS apps into one place, and makes ISO from it.

## Usage
Once repo is downloaded you can simply run  
```bash
$ make iso
```

for further references please see `make`

## License
GNU General Public License v3.0
